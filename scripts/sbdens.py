#!/usr/bin/env python
"""
Calculate in-situ density from a Seabird CNV file.
"""
import sys
import argparse
from dpdata.sci import process_ctd
from dpdata.readcnv import readcnv


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('infile',
                        nargs='?',
                        type=argparse.FileType('rb'),
                        default=sys.stdin,
                        help='input CNV file')
    parser.add_argument('outfile',
                        nargs='?',
                        type=argparse.FileType('wb'),
                        default=sys.stdout,
                        help='output CSV file')
    args = parser.parse_args()

    df = readcnv(args.infile)
    df.rename(columns={'prDM': 'preswat',
                       't090C': 'tempwat',
                       'sal00': 'pracsal'}, inplace=True)
    ctd = process_ctd(df,
                      lat=df.latitude.mean(),
                      lon=df.longitude.mean())
    ctd.to_csv(args.outfile,
               columns=['preswat', 'tempwat', 'pracsal', 'density'],
               index=False)


if __name__ == '__main__':
    main()
