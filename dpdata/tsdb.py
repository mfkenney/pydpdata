#!/usr/bin/env python
"""
.. module:: tsdb
   :synopsis: Interface to Influx Time-Series Database
"""
import pandas as pd
from influxdb import DataFrameClient


def get_dataset(client, table, dpid, t_start, t_end, tags=None):
    """
    Return a data-set from the database.

    :param client: InfluxDBClient instance
    :param table: measurement name
    :type table: str
    :param dpid: Deep Profiler vehicle ID
    :type tags: int
    :param t_start: start time
    :type t_start: :class:`datetime.datetime`
    :param t_end: end time
    :type t_end: :class:`datetime.datetime`
    :param tags: additional tags to select for
    :type tags: dict
    :rtype: :class:`pandas.DataFrame`
    """
    query = "select * from {} where dpcid=$dpid and time >= $start and time <= $end;".format(table)
    params = {"start": t_start.strftime('%Y-%m-%dT%H:%M:%SZ'),
              "dpid": str(dpid),
              "end": t_end.strftime('%Y-%m-%dT%H:%M:%SZ')}

    rs = client.query(query, bind_params=params, epoch='u')
    df = pd.DataFrame(list(rs.get_points(tags=tags)))

    df.rename(columns={'time': 'timestamp'}, inplace=True)
    df.timestamp = pd.to_datetime(df.timestamp, unit='us')
    df.set_index('timestamp', inplace=True)
    df.index = df.index.tz_localize('UTC')
    df.index.name = None
    return df


def put_dataset(client, table, tags, df):
    """
    Write a data-set to the database.

    :param client: InfluxDBClient instance
    :param table: measurement name
    :type table: str
    :param tags: tag names and values
    :type tags: dict
    :param df: data-set to write
    :type df: :class:`pandas.DataFrame`
    """
    DataFrameClient.write_points(client, df, table, tags=tags,
                                 time_precision='u')
