#!/usr/bin/env python
"""
Read a Seabird CNV file.
"""
from pandas import read_csv, to_datetime
import numpy as np
import time
from calendar import timegm


def getheader(f):
    """
    Iterate over the header lines in a CNV file.
    """
    for line in f:
        if line.startswith('*END*'):
            break
        yield line


def readcnv(infile):
    """
    Read the contents of a CNV file into a DataFrame.
    """
    mult = {
        'seconds': 1,
        'minutes': 60,
        'hours': 3600
    }
    labels = []
    interval = 0
    start_time = 0
    for line in getheader(infile):
        if line.startswith('# name'):
            _, name = line.strip().split('=')
            label, desc = name.split(':')
            labels.append(label.lstrip())
        elif line.startswith('# interval'):
            units, val = line[12:].strip().split(':')
            if units in mult:
                interval = float(val) * mult[units]
        elif line.startswith('# start_time'):
            st, _ = line[14:].split('[')
            start_time = timegm(time.strptime(st.strip(), '%b %d %Y %H:%M:%S'))
    if labels:
        df = read_csv(infile, sep='\s+', names=labels,
                      engine='python')
        if interval > 0:
            t = to_datetime(np.linspace(start_time,
                                        start_time + len(df) * interval,
                                        num=len(df)),
                            unit='s')
            df = df.assign(timestamp=t)
        return df
    return None
