#!/usr/bin/env python
"""
Import the contents of a Deep Profiler MessagePack archive
file into an InfuxDB time-series database.
"""
from __future__ import print_function
import os
import argparse
from dpdata import expand_lists
from dpdata.mpk import get_records
from influxdb import InfluxDBClient


def gen_points(f, xtags):
    filename, _ = os.path.splitext(os.path.basename(f.name))
    sens, index, pstart, pnum = filename.split('_')
    for secs, usecs, data in get_records(f):
        if isinstance(data, dict):
            ts = int(secs * 1000000) + usecs
            tags = {'index': index, 'profile': pnum}
            tags.update(xtags)
            if sens == 'mmp':
                tags['pmode'] = data['mode']
                del data['mode']
            elif sens == 'profiler':
                tags['pmode'] = data['state']
                del data['state']
            yield {
                'measurement': sens,
                'time': ts,
                'tags': tags,
                'fields': expand_lists(data)
            }


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('dbname', help='database name')
    parser.add_argument('infiles', help='input files',
                        type=argparse.FileType('rb'),
                        nargs='+')
    parser.add_argument('--host',
                        default='localhost',
                        help='Influx database host')
    parser.add_argument('--port', '-p',
                        type=int,
                        default=8086,
                        help='Influx database port')
    parser.add_argument('--credentials', '-c',
                        metavar='USER:PASSWORD',
                        help='user credentials for database access')
    parser.add_argument('--tag', '-t',
                        action='append',
                        default=[],
                        metavar='KEY=VALUE',
                        help='set a tag for each measurement')
    args = parser.parse_args()

    if args.credentials:
        if args.credentials.startswith('@'):
            with open(args.credentials[1:], 'r') as f:
                user, pword = f.readline().strip().split(':')
        else:
            user, pword = args.credentials.split(':')
    else:
        user, pword = 'guest', 'guest'

    xtags = dict([t.split('=') for t in args.tag])
    client = InfluxDBClient(args.host, args.port, user, pword)
    client.switch_database(args.dbname)

    for f in args.infiles:
        print('Parsing {} ...'.format(f.name))
        client.write_points(list(gen_points(f, xtags)), time_precision='u')


if __name__ == '__main__':
    main()
